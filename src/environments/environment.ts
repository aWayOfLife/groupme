// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCzSWZRz7ecAWUWwUdlryJOSk4OH5H0-PY",
    authDomain: "groupme-437af.firebaseapp.com",
    projectId: "groupme-437af",
    storageBucket: "groupme-437af.appspot.com",
    messagingSenderId: "608695467835",
    appId: "1:608695467835:web:5c4cc77c83955403ac011e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
