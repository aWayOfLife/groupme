import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AppService } from '../app.service';
import { IUser } from '../interfaces/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users:IUser[];
  sortOptions =[
    {name:'Most Relevant', value:'relevant'},
    {name:'Name Ascending', value:'nameAsc'},
    {name:'Name Descending', value:'nameDsc'},
  ];


  constructor(private appService:AppService) { }

  ngOnInit(): void {
    this.getAllUsers('relevant');
  }

  getAllUsers(sort: string){
    this.appService.getAllUsers().subscribe(result=>{
      this.users = result;
      switch (sort){
        case 'nameAsc':
          this.users = result.sort((a,b)=>{ return a.name.localeCompare(b.name)});
          break;
        case 'nameDsc':
          this.users = result.sort((a,b)=>{ return a.name.localeCompare(b.name)}).reverse();
          break;
        case 'relevant':
          this.users = result;
          break;
      }

    },error=>{
      console.log(error);
    })
  }

  onSortSelected(sort:string){
    this.getAllUsers(sort);
  }


}
