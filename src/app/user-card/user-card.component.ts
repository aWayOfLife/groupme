import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { IUser } from '../interfaces/user';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {
  @HostBinding('style.--target-thickness') private targetThickness: string;
  @HostBinding('style.--target-color') private targetColor: string;
  @Input() user:IUser
  constructor() { }

  ngOnInit(): void {
    this.targetThickness = 2+"px";
    this.targetColor = "#efefef"
  }

  check(){
    if(this.user.selected){
      
      this.user.selected = false;
      this.targetThickness = 2+"px";
      this.targetColor = "#efefef"
    }
    else{
      
      this.user.selected = true;
      this.targetThickness = 3+"px";
      this.targetColor = "#2caffe";
    }
  }

}
