import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {
  file: File = null;
  url: any;
  name: string;
  description:string;

  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onSelect(event) {
    if (event.addedFiles && event.addedFiles[0]) {

      this.file = event.addedFiles[0];
      var reader = new FileReader();

      reader.readAsDataURL(event.addedFiles[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
    }
  }
  
  onRemove(event) {
    this.file = null;
    this.url = null;
  }
  
  showSuccess() {
    if(this.name!=null && this.description!=null){
      this.toastr.success('members added successfully','Group Created');
    }
    else{
      this.toastr.error('there are missing fields','Group Creation Failed');
    }
    
    
  }

}
