import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  baseUrl: string = 'https://s3-ap-southeast-1.amazonaws.com/he-public-data/users49b8675.json'

  constructor(private http:HttpClient) { }

  getAllUsers():Observable<IUser[]>{
    return this.http.get<IUser[]>(this.baseUrl);
  }
}
